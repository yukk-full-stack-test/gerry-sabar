import { defineStore } from 'pinia'

export const useUserStore = defineStore({
    id: 'user',
    state: () => ({
      currentUser: {},
    }),
    persist: true,
  
    actions: {
      addUser(user) {
        this.currentUser = user;
      },
      removeUser() {
        this.currentUser = {};
      },
    },  
  })