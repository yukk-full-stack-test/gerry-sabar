<?php
namespace Tests\Feature\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class AuthControllerTest extends TestCase
{

    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */

    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_user_register()
    {
        $response = $this->postJson('/api/register', [
            "name" => "Test Account",
            "email" => "test@test.com",
            "password" => "password",
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'name',
                'email',
                'updated_at',
                'created_at',
                'id',
            ],
            'access_token',
            'token_type'
        ]);
    }

    public function test_user_register_identical_email() {
        $response = $this->postJson('/api/register', [
            "name" => "Test Account",
            "email" => "test@test.com",
            "password" => "password",
        ]);

        $response = $this->postJson('/api/register', [
            "name" => "Test Account",
            "email" => "test@test.com",
            "password" => "password",
        ]);

        $response->assertStatus(403);
        $response->assertJsonStructure([
            'email'
        ]);
    }

    public function test_user_login() {
        User::factory()->create([
            'name' => 'test user',
            'email' => 'test@test.com',
            'password' => bcrypt('password')
        ]);

        $response = $this->postJson('/api/login', [
            "email" => "test@test.com",
            "password" => "password",
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                'user' => [
                    'id',
                    'name',
                    'email',
                    'email_verified_at',
                    'balance',
                    'created_at',
                    'updated_at',
                ],
                'access_token',
                'token_type',
            ],
        ]);
    }

    public function test_user_login_failed() {
        User::factory()->create([
            'name' => 'test user',
            'email' => 'test@test.com',
            'password' => bcrypt('password')
        ]);

        $response = $this->postJson('/api/login', [
            "email" => "test@test.com",
            "password" => "wrong_password",
        ]);

        $response->assertStatus(402);
        $response->assertJsonStructure([
            'message'
        ]);
    }

    public function test_user_logout() {
        User::factory()->create([
            'name' => 'test user',
            'email' => 'test@test.com',
            'password' => bcrypt('password')
        ]);

        $userResponse = $this->postJson('/api/login', [
            "email" => "test@test.com",
            "password" => "password",
        ]);

        $content = json_decode($userResponse->getContent());
        $response = $this->withHeaders([
            'Authorization' => 'Bearer ' . $content->data->access_token
        ])->postJson('/api/logout', [
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'message'
        ]);
    }

}
