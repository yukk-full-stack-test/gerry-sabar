import "./bootstrap";
import { createApp } from "vue/dist/vue.esm-bundler";
import { createRouter, createWebHashHistory, createWebHistory } from "vue-router";

import App from "./App.vue";
import {createPinia} from 'pinia';
import piniaPluginPersistedState from "pinia-plugin-persistedstate";
import {useUserStore} from "./stores/user"
import Register from "./pages/Register.vue";
import Login from "./pages/Login.vue";
import TransactionListView from "./pages/user/TransactionListView.vue";
import UserTransactionCreate from "./pages/user/UserTransactionCreate.vue";

const routes = [
  { path: "/", component: TransactionListView, name: "index", meta: {authRequired: true} },
  { path: "/register", component: Register, name: "register" },
  { path: "/login", component: Login, name: "login" },
  { 
    path: "/user/transaction/list",
    component: TransactionListView,
    name: "transactionListView",
    meta: {
      authRequired: true
    }
  },
  { 
    path: "/user/transaction/create",
    component: UserTransactionCreate,
    name: "userTransactionCreate",
    meta: {
      authRequired: true
    }
  },
];
const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (!to.meta.authRequired) {
    next();
  }

  const userStore = useUserStore();
  if (!userStore.currentUser.access_token) {
    next("/login");
  } 
  
  next();
});

const pinia = createPinia();
pinia.use(piniaPluginPersistedState)
const app = createApp(App);
app.use(pinia);
app.use(router);
app.mount("#app");
