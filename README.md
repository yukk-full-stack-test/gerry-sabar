
## Installation:
- Clone the repository
- Go to the root directory and run command ```composer install``` followed by ```npm install```
- Create two databases in mysql, one for development named laravel_vue and another one for test named test_laravel_vue (or another name you like & adjusted in .env & phpunit.xml file as well)
- Adjust the DB connection by creating a .env file and followed by adjusting phpunit.xml to comply with your local mysql setting. Please refer to .env.example for .env configuration
- after database is created and connected, to migrate development database you can run command ```php artisan migrate``` while to migrate testing database you can run command ```php artisan migrate --database=testing```
- database seeder is provided by execute for development environment ```php artisan db:seed```

## Running app:
- from the root project run ```php artisan serve``` (the app will run at localhost:8000)
- open another terminal and from the root project run ```npm run dev``` for front-end
- you can run the app from browser by accessing http://localhost:8000

## Project Overview
You can try login with email ```user@user.com``` and password ```password``` if you run the seeder. This address the concept and purpose of migration and seeder. After login there are 2 pages for dashboard page. One is transaction list which can be accessed at http://localhost:8000/user/transaction/list as follow:

![alt text](https://i.ibb.co/2WmmBF3/image.png)

You can click ```Create New Transaction``` button to add a new transaction for respective user. Any debit transaction is denoted by positive number i.e.: 2000 while credit transaction is denoted by negative number i.e.: -2000. While top-up will automatically considered as debit. There is no edit or delete the transaction considering the nature of financial flow to keep record any balance change. Therefore if there's any wrong input then another adjusted transaction is added to revert the process. For example this condition:

1. A user input transaction with amount 1000 mistakenly
2. Then another transaction will be created with amount -1000 to revert the mistake from step 1

At transaction list page user balance will be adjusted right after any new transaction is committed. 

## Initial Assumption
While trying to address certain priciples as good as and as much as possible but maintaining the time constrain, therefore there are initial assumptions for the project.

### Architecture
To simplify the project repo I put it in monolith architecture but separated the backend and front-end process which a simplify the concept of microservices.

### Balance Calculation
There is no silver bullet for efficient balance calculation especially in huge traffic flow and race condition may occur. Therefore in this case since it's a simple calculation process yet try to address query effieciency for balance calculation I apply a quick reference for user balance at user table in column balance. I also list all transaction process. Then to double check the balance calculation process there is a cronjob to adjust user's balance by sum all transactions which running on daily basis without pay much attention for complex & efficient query process yet address the importance of transact query and efficient balance calculation.

### Atomic Design Principle
Atomic design principle is only cover small components due to maintain the time constrain deadline yet addressed the simple structure to cover this matter.

### Mobile Responsive
Mobile response design is implemented however. There is a glitch on mobile device such as IPhone SE on potrait scale at transaction list page. This is due to the stretching table and further examination takes time while I have to maintain the time constrain deadline. Currently works fine on desktop and tablet device such as IPad. 

### Test Driven Development
Test driven development only cover AuthController due to maintain the time constrain deadline yet addressed the approach to cover this matter which is important phase for Continuous Integration / Continuous Deployment. Front-end component test isn't commited due to the time constrain as well.