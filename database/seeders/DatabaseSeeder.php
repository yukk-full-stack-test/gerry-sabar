<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        Schema::disableForeignKeyConstraints();
        DB::table('users')->truncate();
        DB::table('transactions')->truncate();

        for ($i=0; $i < 3; $i++) {
            User::factory()->create([
                'name' => fake()->name(),
                'email' => 'user' . ($i == 0 ? "" : $i) . '@user.com',
                'password' => bcrypt('password')
            ]);
        }

        $amounts = [100, 200, 400, 800, 1000];
        $types = ['top-up', 'transaksi'];
        $codeType = ['TR', 'TO'];
        $counter = 1;
        $total = 0;
        for ($x=0; $x<15; $x++){
            $transaction = new Transaction();
            $transaction->user_id = 1;
            $transaction->code = $codeType[rand(0,1)] . '1' . $counter;
            $transaction->type = $types[rand(0,1)];
            $transaction->amount = $amounts[rand(0,4)];
            $transaction->save();
            $total += $transaction->amount;
            ++$counter;
        }
        $user = User::find(1);
        $user->balance = $total;
        $user->save();

        $total = 0;
        for ($x=0; $x<15; $x++){
            $transaction = new Transaction();
            $transaction->user_id = 2;
            $transaction->code = $codeType[rand(0,1)] . '2' . $counter;
            $transaction->type = $types[rand(0,1)];
            $transaction->amount = $amounts[rand(0,4)];
            $transaction->save();
            $total += $transaction->amount;
            ++$counter;
        }
        $user2 = User::find(2);
        $user2->balance = $total;
        $user2->save();

        $total = 0;
        for ($x=0; $x<15; $x++){
            $transaction = new Transaction();
            $transaction->user_id = 3;
            $transaction->code = $codeType[rand(0,1)] . '3' . $counter;
            $transaction->type = $types[rand(0,1)];
            $transaction->amount = $amounts[rand(0,4)];
            $transaction->save();
            $total += $transaction->amount;
            ++$counter;
        }
        $user3 = User::find(3);
        $user3->balance = $total;
        $user3->save();

    }
}
