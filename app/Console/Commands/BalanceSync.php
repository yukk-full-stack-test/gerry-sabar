<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Transaction;
use DB;

class BalanceSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'balancesync:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        DB::beginTransaction();
        $users = User::all();
        foreach($users as $user) {
            $transactions = Transaction::where('user_id', $user->id)->get();
            $totalTransaction = 0;
            foreach($transactions as $transaction){
                $totalTransaction += $transaction->amount;
            }
            $user->balance = $totalTransaction;
            $user->save();
        }
        DB::commit();
        
        \Log::info("Users' balance adjustment executed successfully at " . date('Y-m-d H:i:s'));
    }
}
