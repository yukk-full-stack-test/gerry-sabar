<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use App\Models\Transaction;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends Controller
{

    public function createTransaction(Request $request)
    {

        $user = Auth::user();
        if ($request->hasFile('file')){
            $fileName = Str::uuid()->getHex() . '.' . $request->file->getClientOriginalExtension();
            $request->file->move(public_path("upload/{$user->id}/"), $fileName);
        }

        $validator = Validator::make($request->all(), [
            'type' => 'required|string',
            'amount' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 403);
        }

        DB::beginTransaction();
        $user->balance += $request->amount;
        $user->save();

        $trCode = $request->type == 'top-up' ? 'TO' : 'TR';
        $trCode .= $user->id . Transaction::count();
        $transaction = new Transaction();
        $transaction->user_id = $user->id;
        $transaction->code = $trCode;
        $transaction->type = $request->type;
        $transaction->amount = $request->amount;
        if ($request->hasFile('file')){
            $transaction->file = $fileName;
        }
        $transaction->description = $request->description;
        $transaction->save();    
        DB::commit();

        return response()->json([
            'data' => $transaction,

        ], 200);
    }

    public function index(Request $request){
        $transactions = new Transaction();
        $user = Auth::user();
        if ($request->search){
            $transactions = $transactions->where(function($q) use($request) {
                $q->where('type', 'like', '%' . $request->search . '%')
                  ->orWhere('description', 'like', '%' . $request->search . '%');
            });
        }

        $transactions = $transactions->where('user_id', $user->id);

        if ($request->filter){
            $transactions = $transactions->orderBy($request->filter, $request->order);
        }

        return response()->json([
            'transactions' => $transactions->paginate(5),
            'base_url' => url("/upload/$user->id"),
        ], 200);

        return $transactions->paginate(5);
    }

    public function search(Request $request)
    {
        $transactions = Transaction::where(function($q) use($request) {
            $q->where('type', 'like', '%' . $request->search . '%')
              ->orWhere('description', 'like', '%' . $request->search . '%');
        });

        if ($request->filter){
            $transactions = $transactions->orderBy($request->filter, $request->order);
        }
        
        $user = Auth::user();
        $transactions = $transactions->where('user_id', $user->id)->paginate(5);

        return response()->json($transactions, 200);

    }


}
